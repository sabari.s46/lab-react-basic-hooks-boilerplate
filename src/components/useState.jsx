import React, { useContext, useState, useEffect } from "react";
import { ThemeChange } from "../App";

function UseContext() {
  const theme = useContext(ThemeChange);
  const themeStyle = {
    backgroundColor: theme ? "black" : "grey",
    color: theme ? "grey" : "black",
    padding: "2rem",
    margin: "2rem",
  };
  const [count, setCount] = useState(0);
  const [context, setContext] = useState(" ");

  useEffect(() => {
    if (context !== " ") {
      alert("Context button is clicked");
    }
  }, [context]);

  const changeContext = () => {
    if (context !== " ") {
      setContext(" "); // Update the context state
    } else {
      setContext(
        `Lorem ipsum dolor sit amet consectetur adipisicing elit. Exercitationem itaque quasi maiores ex nostrum deleniti obcaecati! Id porro voluptas illum facilis dolorem ea, ipsam, expedita nostrum ullam veritatis, eum minus!`
      );
    }
  };

  return (
    <div>
      <div style={themeStyle}>
        This is made by Kalvium
        <div className="Context">
          <p>{context}</p>
          <button onClick={changeContext}>Context</button>
        </div>
        <div className="Counter">
          <p>{count}</p>
          <button onClick={() => setCount((count) => count + 1)}>Like</button>
        </div>
      </div>
    </div>
  );
}

export default UseContext;
