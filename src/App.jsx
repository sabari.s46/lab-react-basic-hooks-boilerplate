import React, { useState } from "react"; // Correct import statement
import "./App.css";
import UseContext from "./components/useState";

export const ThemeChange = React.createContext();

function App() {
  const [state, setState] = useState(true);
  const handleToggle = () => {
    setState((state) => !state);
  };
  return (
    <div className="Boss">
      <ThemeChange.Provider value={state}>
        <button className="Button" onClick={handleToggle}>
          Toggle
        </button>
        <UseContext />
      </ThemeChange.Provider>
    </div>
  );
}

export default App;
